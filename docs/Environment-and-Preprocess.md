# 环境依赖

- Python 3.7.1
- Jupyter Notebook（client 5.2.4 core 4.4.0）
- Pandas 0.23.4 [documentation](http://pandas.pydata.org/pandas-docs/version/0.23.4/)

我们使用 Python 作为脚本语言，以 Jupyter Notebook 作为 Shell，使用 Pandas 库进行数据分析和处理。

> 后续的模型训练会根据模型不同依赖相应框架

# 数据预处理

对应 Notebook：[../Preprocess.ipynb](../Preprocess.ipynb)

## 数据格式选用

> 详见[数据描述](Description-and-Data.md#数据描述)

我们将会抛弃所有 `label` 为 -1 的数据，显然数据格式 format2 更适合这种解决方案。

## 合并训练集与测试集

我们将会对训练集和测试集一同进行预处理和后续的特征工程，当特征工程完成后，再根据 label 为 NaN 从并集中分离出测试集。

## 删除 `label` 为 -1 的行

*'-1' 表示该用户不是该商家的新买家，因此超出了我们的预测范围。但是，这种记录可能提供其他信息。*

我们基于以下两点决定删除 label 值为 -1 的行：

1. 暂时还未想好如何利用“其他信息”；
2. 数据太多，后续处理将会花费大量内存和时间。

## 处理 `age_range`

将 8 合并到 7，表示年龄范围为 >=50；将 NaN 合并到 0 表示未知年龄范围。

## 处理 `gender`

将 NaN 合并到 2 表示未知性别。

## 处理 `activity log`

先将 NaN 填充为空字符串，再统一根据 '#' 和 ':' 进行两层分割得到嵌套列表。

> 这个步骤需要特殊注意空字符串到空列表的处理。

得到嵌套列表后，再根据定义将五个字段分别提取到新的列表中，成为新的列/属性。最后删除原 `activity log`。
