# 天池新人赛 回头客预测之模型训练—— Xgboost梯度提升决策树模型
姓名：王烁东            
学号：16051326

---

## 第一部分---Xgboost模型模型基本介绍
XGBoost 是 “Extreme Gradient Boosting”的简称，中文叫做梯度提升决策树，简称GBDT，同时也有简称GBRT，GBM。
XGBoost 主要是用来解决有监督学习问题，此类问题利用包含多个特征的训练数据 x ，来预测目标变量 y。

### 复合树模型
复合树模型是xgboost所对应的模型。复合树模型是一组分类回归树。
 
复合树模型把家庭中的成员分到了不同的叶子节点，同时每个叶子节点上都有一个分数。CART与决策树相比，细微差别在于CART的叶子节点仅包含判断分数。在CART中，相比较于分类结果，每个叶子节点的分数给我们以更多的解释。这让CART统一优化节点更为容易。
通常情况下，在实践中往往一棵树是不够用的。这个时候往往需要把多棵树的预测结果综合起来，这就是所谓的复合树模型。


Xgboost的优点：
* 1、正则化
标准GBM的实现没有像XGBoost这样的正则化步骤。正则化对减少过拟合也是有帮助的。 实际上，XGBoost以“正则化提升(regularized boosting)”技术而闻名。
* 2、并行处理
XGBoost可以实现并行处理，相比GBM有了速度的飞跃。 不过，众所周知，Boosting算法是顺序处理的，它怎么可能并行呢？每一课树的构造都依赖于前一棵树，那具体是什么让我们能用多核处理器去构造一个树呢？我希望你理解了这句话的意思。 XGBoost 也支持Hadoop实现。
* 3、高度的灵活性
XGBoost 允许用户定义自定义优化目标和评价标准 它对模型增加了一个全新的维度，所以我们的处理不会受到任何限制。
* 4、缺失值处理
XGBoost内置处理缺失值的规则。 用户需要提供一个和其它样本不同的值，然后把它作为一个参数传进去，以此来作为缺失值的取值。XGBoost在不同节点遇到缺失值时采用不同的处理方法，并且会学习未来遇到缺失值时的处理方法。
* 5、剪枝
当分裂时遇到一个负损失时，GBM会停止分裂。因此GBM实际上是一个贪心算法。 XGBoost会一直分裂到指定的最大深度(max_depth)，然后回过头来剪枝。如果某个节点之后不再有正值，它会去除这个分裂。 这种做法的优点，当一个负损失（如-2）后面有个正损失（如+10）的时候，就显现出来了。GBM会在-2处停下来，因为它遇到了一个负值。但是XGBoost会继续分裂，然后发现这两个分裂综合起来会得到+8，因此会保留这两个分裂。
* 6、内置交叉验证
XGBoost允许在每一轮boosting迭代中使用交叉验证。因此，可以方便地获得最优boosting迭代次数。 而GBM使用网格搜索，只能检测有限个值。
* 7、在已有的模型基础上继续
XGBoost可以在上一轮的结果上继续训练。这个特性在某些特定的应用上是一个巨大的优势。 sklearn中的GBM的实现也有这个功能，两种算法在这一点上是一致的。


---
## 第二部分---Xgboost梯度提升决策树模型参数
只有了解参数的作用才能开始对模型训练进行调试。

### 通用参数
|                                参数 | 定义及作用                                                            |
| ----------------------------------: | --------------------------------------------------------------------- |
|               `booster[默认gbtree]` | 选择每次迭代的模型，有两种选择：gbtree：基于树的模型gbliner：线性模型 |
|                     `silent[默认0]` | 静默模式，当这个参数值为1时，静默模式开启，不会输出任何信息。         |
| `nthread[默认值为最大可能的线程数]` | 用来进行多线程控制，应当输入系统的核数。                              |

### 部分booster参数(tree booster)

|                      参数 | 定义及作用                                                                                                                    |
| ------------------------: | ----------------------------------------------------------------------------------------------------------------------------- |
|            `eta[默认0.3]` | 与learning rate 参数类似。 通过减少每一步的权重，可以提高模型的鲁棒性。 典型值为0.01-0.2。                                    |
| `min_child_weight[默认1]` | 决定最小叶子节点样本权重和。用于避免过拟合。值较大时，可以避免模型学习到局部的特殊样本，但值过高时，会导致欠拟合。            |
|        `max_depth[默认6]` | 树的最大深度。用于避免过拟合的。max_depth越大，模型会学到更具体更局部的样本。典型值：3-10                                     |
|          `max_leaf_nodes` | 树上最大的节点或叶子的数量。 可以替代max_depth的作用。                                                                        |
|            `gamma[默认0]` | 在节点分裂时，若分裂后损失函数的值下降了，分裂这个节点。Gamma指定了节点分裂所需的最小损失函数下降值。值越大，算法越保守。     |
|        `subsample[默认1]` | 控制对每棵树随机采样的比例。 减小这个参数时，算法会更加保守，避免过拟合。若这个值设置得过小，可能会导致欠拟合。 典型值：0.5-1 |

### 学习目标参数
|                                           参数 | 定义及作用                                                                               |
| ---------------------------------------------: | ---------------------------------------------------------------------------------------- |
|                    `objective[默认reg:linear]` | 定义需要被最小化的损失函数。常用值有：binary:logistic 二分类的逻辑回归，返回预测的概率。 |
| `eval_metric[默认值取决于objective参数的取值]` | 对于有效数据的度量方法。 对于回归问题，默认值是rmse，对于分类问题，默认值是error。       |
|                                  `seed(默认0)` | 随机数的种子 设置它可以复现随机数据的结果，也可以用于调整参数                            |



## 第三部分---梯度提升决策树模型的调用
`Xgboost`梯度提升决策树的代码:

```
import pandas as pd
import xgboost as xgb
from sklearn.cross_validation import train_test_split
from sklearn import metrics

train = pd.read_pickle('DataSet/train.pkl')
test = pd.read_pickle('DataSet/test.pkl')

train_xy,val = train_test_split(train, test_size = 0.25,random_state=1)

y = train_xy.label
X = train_xy.drop(['label'],axis=1)
val_y = val.label
val_X = val.drop(['label'],axis=1)

#xgb矩阵赋值
xgb_val = xgb.DMatrix(val_X,label=val_y)
xgb_train = xgb.DMatrix(X, label=y)
xgb_test = xgb.DMatrix(test)

#Xgboost参数及其调试
xgb_params = {
    'seed': 0,
    'colsample_bytree': 0.8,    # 0.5  0.6  0.7  0.8  0.9
    'silent': 1,
    'subsample': 0.5,
    'learning_rate': 0.05,
    'objective': 'reg:logistic',
    'max_depth': 5,     # 最优  4 、5
    'num_parallel_tree': 1,
    'min_child_weight': 1,
    'loss': 'reg:logistic',
    'eval_metric': 'auc',
    'max_delta_step': 0
}

plst = list(xgb_params.items())
num_rounds = 5000 # 迭代次数
watchlist = [(xgb_train, 'train'),(xgb_val, 'val')]

#训练模型并保存
# early_stopping_rounds 当设置的迭代次数较大时，early_stopping_rounds 可在一定的迭代次数内准确率没有提升就停止训练
model = xgb.train(plst, xgb_train, num_rounds, watchlist,early_stopping_rounds=100)

#预测train概率
metrics.roc_auc_score(y, model.predict( xgb_train))
```
---

## 个人总结

在本学期的创新实践中，选择了数据挖掘这一课题，在实际学习中，学会的主要是数据的前期处理部分的工作。对于数据的预处理以及特征工程部分的相关工作有了初步的了解，较为不足是对于模型训练部分的理解还很粗浅，其中有个人时间规划的因素，于数学、英语等方面的基础薄弱也造成了一些困难，希望在此后的学习中可以裨补缺漏，有所提升。






