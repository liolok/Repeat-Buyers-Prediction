# 天池新人赛 回头客预测之特征工程 
姓名：王烁东            
学号：16051326

---

## 第一部分---特征工程概述
 “数据决定了机器学习的上限，而算法只是尽可能逼近这个上限”，这里的数据指的就是经过特征工程得到的数据。特征工程指的是把原始数据转变为模型的训练数据的过程，它的目的就是获取更好的训练数据特征，使得机器学习模型逼近这个上限。特征工程能使得模型的性能得到提升，有时甚至在简单的模型上也能取得不错的效果。特征工程在机器学习中占有非常重要的作用，一般认为括特征构建、特征提取、特征选择三个部分。特征构建比较麻烦，需要一定的经验。 特征提取与特征选择都是为了从原始特征中找出最有效的特征。它们之间的区别是特征提取强调通过特征转换的方式得到一组具有明显物理或统计意义的特征；而特征选择是从特征集合中挑选一组具有明显物理或统计意义的特征子集。两者都能帮助减少特征的维度、数据冗余，特征提取有时能发现更有意义的特征属性，特征选择的过程经常能表示出每个特征的重要性对于模型构建的重要性。

![特征工程](images/Feature-Engineer/1.png)    

特征构建是指从原始数据中人工的找出一些具有物理意义的特征。需要花时间去观察原始数据，思考问题的潜在形式和数据结构，对数据敏感性和机器学习实战经验能帮助特征构建。除此之外，属性分割和结合是特征构建时常使用的方法。结构性的表格数据，可以尝试组合二个、三个不同的属性构造新的特征，如果存在时间相关属性，可以划出不同的时间窗口，得到同一属性在不同时间下的特征值，也可以把一个属性分解或切分，例如将数据中的日期字段按照季度和周期后者一天的上午、下午和晚上去构建特征。


---
## 第二部分---项目特征描述

提供数据描述:

对于本次竞赛，比赛官方提供以下数据：用户的人口统计信息，“双11”促销之前六个月的用户活动日志数据，以及培训和测试新的购买者，商品对，第一次从商家购买新买家是在“双11”促销。用户人口统计数据包含用户的年龄和性别。年龄值分为七个范围。培训重新购买者，商品对的类别标签是已知的，并且它指示新购买者是否在“双11”促销之后的六个月内再次从商家购买商品。测试新买家，商品对的类标签是隐藏的。任务是预测测试对的类标签。
用户活动日志数据包含以下字段：用户ID，商家ID，商品ID，cat id，品牌ID，操作类型和时间戳。 操作类型有四个值：0表示点击，1表示添加到购物车，2表示购买，3表示添加到收藏夹。 即使产品完全相同，在不同商家销售的产品也会分配不同的商品ID。 表2显示了用户活动日志数据的统计信息。 日志数据中的许多商家在培训或测试数据中没有新买家。 它们包含在日志数据中，因为一些新买家访问了它们。 这些商家的新买家的活动是用于推断新买家的偏好和习惯的有价值的信息。用户行为记录表中显示了四种类型的操作的数量。 大多数操作都是点击操作。
竞赛中提供的用户活动日志数据在电子商务预测任务中是非常典型的。 但是，日志数据的形式不适合学习。 我们需要从中构建新功能，然后将新功能与训练和测试数据结合起来。 

用户活动日志数据包含五个实体：用户，商家，品牌，类别和项目。 这些实体的特征及其相互作用可以预测类标签。 例如，用户更有可能从销售零食的商家再次购买，而不是在六个月内从销售电子产品的商家购买，因为零食比电子产品更便宜且消费更快。

---
## 第三部分---特征工程文件描述

### 用户特征：

* 1) `age_range`: 用户年龄特征；
* 2) `gender` :用户性别特征；
  
![用户特征-1](images/Feature-Engineer/2.png) 

* 3) `Count Log Each Month`: 用户行为月计数;
* 4) `Count Log Each Month add_cart`: 用户添加购物车行为月计数;
* 5) `Count Log Each Month purchase`: 用户购买行为月计数;
* 6) `Count Log Each Month add_fav`: 用户添加收藏行为月计数;
* 7) `Count Log Each Month click`: 用户点击行为月计数;

![用户特征-2](images/Feature-Engineer/3.png) 

* 8) `Average Action Types Count click_u_avg`: 用户点击行为月平均计数；
* 9) `Average Action Types Count add_cart_u_avg`: 用户添加购物车行为月平均计数；
* 10) `Average Action Types Count purchase_u_avg`: 用户购买行为月平均计数；
* 11) `Average Action Types Count add_fav_u_avg`: 用户添加收藏行为月平均计数；

![用户特征-3](images/Feature-Engineer/4.png) 

### 商家特征
* 1) `Customer Age Range Vectors`:商家-用户年龄范围；
* 2) `Customer Gender Vectors` :商家-用户性别比；
* 3) `Average Monthly Log Count`商家-用户行为月计数;

### 商家-用户特征：
* 1) `Log Count Ratio`商家-用户行为月计数比;

### 其他特征：
* 1) `Item Count`：项目-用户行为月计数;
* 2) `Category Count`: 类别-用户行为月计数;
* 3) `Brand Count`: 品牌-用户行为月计数;

![其他特征-1](images/Feature-Engineer/5.png) 

* 1) `Item Count Ratio`：项目-用户行为月计数比;
* 2) `Category Count Ratio`: 类别-用户行为月计数比;
* 3) `Brand Count Ratio`: 品牌-用户行为月计数比;

![其他特征-2](images/Feature-Engineer/6.png) 

### 特征总览：

|               特征名 | 特征数据总量 | 状态     | 定义    |
| -------------------: | ------------ | -------- | ------- |
|              user_id | 522341       | non-null | int64   |
|          merchant_id | 522341       | non-null | int64   |
|                label | 260864       | non-null | float64 |
|            log_count | 522341       | non-null | int64   |
|          age_range_0 | 522341       | non-null | uint8   |
|          age_range_1 | 522341       | non-null | uint8   |
|          age_range_2 | 522341       | non-null | uint8   |
|          age_range_3 | 522341       | non-null | uint8   |
|          age_range_4 | 522341       | non-null | uint8   |
|          age_range_5 | 522341       | non-null | uint8   |
|          age_range_6 | 522341       | non-null | uint8   |
|          age_range_7 | 522341       | non-null | uint8   |
|             gender_0 | 522341       | non-null | uint8   |
|             gender_1 | 522341       | non-null | uint8   |
|             gender_2 | 522341       | non-null | uint8   |
|                log_5 | 522341       | non-null | int64   |
|                log_6 | 522341       | non-null | int64   |
|                log_7 | 522341       | non-null | int64   |
|                log_8 | 522341       | non-null | int64   |
|                log_9 | 522341       | non-null | int64   |
|               log_10 | 522341       | non-null | int64   |
|               log_11 | 522341       | non-null | int64   |
|                click | 522341       | non-null | int64   |
|             add_cart | 522341       | non-null | int64   |
|             purchase | 522341       | non-null | int64   |
|              add_fav | 522341       | non-null | int64   |
|          click_u_avg | 522341       | non-null | float64 |
|       add_cart_u_avg | 522341       | non-null | float64 |
|       purchase_u_avg | 522341       | non-null | float64 |
|        add_fav_u_avg | 522341       | non-null | float64 |
|    age_range_0_m_avg | 522341       | non-null | float64 |
|    age_range_1_m_avg | 522341       | non-null | float64 |
|    age_range_2_m_avg | 522341       | non-null | float64 |
|    age_range_3_m_avg | 522341       | non-null | float64 |
|    age_range_4_m_avg | 522341       | non-null | float64 |
|    age_range_5_m_avg | 522341       | non-null | float64 |
|    age_range_6_m_avg | 522341       | non-null | float64 |
|    age_range_7_m_avg | 522341       | non-null | float64 |
|       gender_0_m_avg | 522341       | non-null | float64 |
|       gender_1_m_avg | 522341       | non-null | float64 |
|       gender_2_m_avg | 522341       | non-null | float64 |
|          log_5_m_avg | 522341       | non-null | float64 |
|          log_6_m_avg | 522341       | non-null | float64 |
|          log_7_m_avg | 522341       | non-null | float64 |
|          log_8_m_avg | 522341       | non-null | float64 |
|          log_9_m_avg | 522341       | non-null | float64 |
|         log_10_m_avg | 522341       | non-null | float64 |
|         log_11_m_avg | 522341       | non-null | float64 |
|          click_m_avg | 522341       | non-null | float64 |
|       add_cart_m_avg | 522341       | non-null | float64 |
|       purchase_m_avg | 522341       | non-null | float64 |
|        add_fav_m_avg | 522341       | non-null | float64 |
|      log_count_u_all | 522341       | non-null | int64   |
|      log_count_ratio | 522341       | non-null | float64 |
|           item_count | 522341       | non-null | int64   |
|       category_count | 522341       | non-null | int64   |
|          brand_count | 522341       | non-null | int64   |
|     item_count_u_all | 522341       | non-null | int64   |
| category_count_u_all | 522341       | non-null | int64   |
|    brand_count_u_all | 522341       | non-null | int64   |
|     item_count_ratio | 522341       | non-null | float64 |
| category_count_ratio | 522341       | non-null | float64 |
|    brand_count_ratio | 522341       | non-null | float64 |


