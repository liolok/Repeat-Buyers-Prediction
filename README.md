# 天猫回头客预测

- [问题定义与数据描述](docs/Description-and-Data.md)
- [环境依赖与数据预处理](docs/Environment-and-Preprocess.md)
- [特征工程](docs/Feature-Engineer.md)
- [XGBoost 模型](docs/Xgboost_Model.md)
- [随机森林模型](docs/RandomForestsModel.md)
